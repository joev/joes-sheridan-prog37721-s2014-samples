﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Week10A.Startup))]
namespace Week10A
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
