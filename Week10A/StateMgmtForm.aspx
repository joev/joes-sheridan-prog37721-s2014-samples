﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="StateMgmtForm.aspx.cs" Inherits="Week10A.StateMgmtForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <table class="nav-justified">
    <tr>
        <td style="width: 151px">
            <asp:Label ID="Label1" runat="server" Text="ViewState Time:"></asp:Label>
        </td>
        <td>
            <asp:Label ID="vsLbl" runat="server"></asp:Label>
        </td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td style="width: 151px">
            <asp:Label ID="Label2" runat="server" Text="Session Time:"></asp:Label>
        </td>
        <td>
            <asp:Label ID="sessionLbl" runat="server"></asp:Label>
        </td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td style="width: 151px">
            <asp:Label ID="Label3" runat="server" Text="Query String Time:"></asp:Label>
        </td>
        <td>
            <asp:Label ID="qsLbl" runat="server"></asp:Label>
        </td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td style="width: 151px">
            <asp:Label ID="Label4" runat="server" Text="Support Email:"></asp:Label>
        </td>
        <td>
            <asp:Label ID="emailLbl" runat="server"></asp:Label>
        </td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td style="width: 151px">&nbsp;</td>
        <td>
            <asp:Button ID="submitBtn" runat="server" OnClick="submitBtn_Click" Text="Submit" />
            <asp:Button ID="redirectBtn" runat="server" OnClick="redirectBtn_Click" Text="Redirect" />
        </td>
        <td>&nbsp;</td>
    </tr>
</table>
</asp:Content>
