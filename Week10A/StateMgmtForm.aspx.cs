﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// ASP.NET Application used to demonstrate 3 major ways of storing state information in ASP.NET specifically ViewState collection, Session 
/// collection and Query Strings.  Also demonstrates the use of the System.Configuration.ConfigurationManager class to
/// retrieve configuration parameters from the Web.config file and how to update the navigation bar in the Master Page to provide links to new pages.
/// </summary>
namespace Week10A
{
    /// <summary>
    /// ASP.NET class used to demonstrate 3 major ways of storing state information in ASP.NET.  The ViewState is intended
    /// to store information (including control state) between Postbacks to the same page.  The Session is intended to store information
    /// during an entire user session across all pages of the site.  Query Strings can be used to convey information between pages using 
    /// a specially coded URL.  Note the difference in behaviour of these storage methods depending on whether this page is loaded due to a Postback (Submit Button), 
    /// freshly loaded (navigate away and back) or due to a redirect which uses a query string.  An updated timestamp is displayed whenever the respective
    /// collection has data available.  Otherwise, "Uninitialized" is displayed to show that the respective collection does NOT have any data available.
    /// This class also uses the System.Configuration.ConfigurationManager class to retrieve a configuration parameter (supportEmail) from the Web.config file 
    /// and updates a label control with the value.
    /// </summary>
    public partial class StateMgmtForm : System.Web.UI.Page
    {
        /// <summary>
        /// Method used to display the timestamp value whenever the respective collection key has data available.  Otherwise, "Uninitialized" is 
        /// displayed to show that the respective collection key does NOT have any data available.  Note that we must always check to ensure that the collection entry
        /// exists before trying to use it. This method also uses the System.Configuration.ConfigurationManager class to retrieve a configuration parameter 
        /// (supportEmail) from the Web.config file and updates a label control with the value.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (ViewState["time"] == null)
                vsLbl.Text = "Unitialized";
            else
                vsLbl.Text = (string)ViewState["time"];

            if (Session["time"] == null)
                sessionLbl.Text = "Unitialized";
            else
                sessionLbl.Text = (string)Session["time"];

            if (Request.QueryString["time"] == null)
                qsLbl.Text = "Unitialized";
            else
                qsLbl.Text = (string)Request.QueryString["time"];

            emailLbl.Text = System.Configuration.ConfigurationManager.AppSettings["supportEmail"];
        }

        /// <summary>
        /// Submit button handler which updates the timestamp value associated with "time" key for the ViewState and Session collections to the current
        /// time.  The associated label controls for the ViewState and Session are also updated to display the time that the Submit button was pressed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void submitBtn_Click(object sender, EventArgs e)
        {
            ViewState["time"] = Session["time"] = DateTime.Now.ToString();
            vsLbl.Text = ViewState["time"].ToString();
            sessionLbl.Text = (string)Session["time"];
        }

        /// <summary>
        /// Redirect button handler which builds a URL that appends a query string which contains the timestamp when the Redirect was initiated to the current 
        /// page URL.  The Response.Redirect method is then called to redirect to the full URL with the query string information.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void redirectBtn_Click(object sender, EventArgs e)
        {
            string time = DateTime.Now.ToString();
            Response.Redirect("StateMgmtForm.aspx?time=" + time);
        }
    }
}