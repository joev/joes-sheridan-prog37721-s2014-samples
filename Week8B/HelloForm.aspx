﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="HelloForm.aspx.cs" Inherits="Week8B.HelloForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Label ID="Label1" runat="server" Text="Name:"></asp:Label>
    <asp:TextBox ID="nameTB" runat="server"></asp:TextBox>
    <br />
    <asp:Button ID="submitBtn" runat="server" OnClick="submitBtn_Click" Text="Submit" />
    <br />
    <asp:Label ID="greetingLbl" runat="server"></asp:Label>
</asp:Content>
