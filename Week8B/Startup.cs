﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Week8B.Startup))]
namespace Week8B
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}

