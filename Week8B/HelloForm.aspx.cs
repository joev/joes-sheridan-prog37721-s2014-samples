﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// ASP.NET Application used to demonstrate a simple "Hello World" type application. 
/// </summary>
namespace Week8B
{
    /// <summary>
    /// ASP.NET Page used to demonstrate a simple "Hello World" type application.  The user is greeted after entering
    /// their name and clicking the submit button.
    /// </summary>
    public partial class HelloForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Displays a greeting to the user based on the name entered in the TextBox control 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void submitBtn_Click(object sender, EventArgs e)
        {
            greetingLbl.Text = "Hello " + nameTB.Text + "!";
        }
    }
}