﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;

namespace Week8B
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}

// ************************************************* Doxygen Home Page Customization *****************************************************

/*! \mainpage PROG 37721 Web Services using .Net and C# Programming - Sample Code Home Page
 *
 * \section intro_sec Introduction
 * Welcome to the Home Page of the Sample Code Documentation for the PROG 37721 Web Services using .Net and C# Programming class at Sheridan Institute of Technology and Advanced Learning!
 * 
 * It is my hope that this documentation will assist students in quickly locating helpful sample code for this course and make 
 * our large Visual Studio Solution more manageable.
 * 
 * Enjoy!
 * 
 * :) Joe<BR><BR>
 * 
 * 
 * \section install_sec Getting Started
 * <ul>
 *  <li>Click the <a href="namespaces.html">Packages</a> tab to get a description of every C# Sample Project in this Course</li>
 *  <li>Click the <a href="annotated.html">Classes</a> tab to get a description of every C# Class used in a Sample Project in this Course</li>
 * </ul>
 * <BR>
 * 
 * \section notes_sec Notes
 * <ul>
 * <li>This documentation was relatively easy to generate using <a href="http://www.doxygen.org">Doxygen</a> because all of our Sample Code Projects make extensive use of C# XML comments "///"</li>
 * <li>You can find this file along with the <a href="http://www.doxygen.org">Doxygen</a> project file in the "Doxygen Doc" folder in our Visual Studio solution.  Right click on "index.html" and select "View in Browser"</li>
 * <li>All of these HTML documentation files are contained in the "doxygen\html" folder of the Sample Code Zip file for this course
 * </ul>
 * <BR>
 * 
 * \section suggestions_sec Suggestions
 * This is a work in progress so please contact me (Joe) if you have suggestions for improvements!
 */
