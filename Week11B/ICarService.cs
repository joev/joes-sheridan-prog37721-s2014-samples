﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

/// <summary>
/// WCF Service Library used to demonstrate providing a WCF Service for applications to consume.  This demo shows how to define a new Composite
/// data type (Car); how to add methods to an existing interface; how to add an interface to an existing web service and how to consume it using the WCF Test Client.
/// </summary>
namespace Week11B
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    /// <summary>
    /// Defines the new ICarService interface for the required method (GetCarDescription).  Please note that the [ServiceContract] attribute is used
    /// before the definition to indicate that the interface defines a WCF Service Contract.  Also note that all of the methods that we want
    /// to include in the service need to be prefixed with the [OperationContract] attribute.
    /// </summary>
    [ServiceContract]
    public interface ICarService
    {
        /// <summary>
        /// Method which provides the interface for building a car description for the specified car
        /// </summary>
        [OperationContract]
        string GetCarDescription(int id);

        /// <summary>
        /// Simple method used to demonstrate the procedure for modifying a service and using the updates.
        /// Step 1: Rebuild the service, Step 2: Update the service reference for the consuming project.
        /// </summary>
        [OperationContract]
        string GetCarServiceVersion(int id);
    }

    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    // You can add XSD files into the project. After building the project, you can directly use the data types defined there, with the namespace "Week11B.ContractType".
    /// <summary>
    /// Simple data contract for the Car composite type.  Notice that the class is prefixed with the [DataContract] attribute while each field which is supposed to 
    /// be exposed to users is prefixed by the [DataMember] attribute.
    /// </summary>
    [DataContract]
    public class Car
    {

        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Make { get; set; }

        [DataMember]
        public string Model { get; set; }

        [DataMember]
        public string Color { get; set; }

        [DataMember]
        public int Year { get; set; }
    }
}
