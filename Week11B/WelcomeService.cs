﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Week11B
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    /// <summary>
    /// Simple WCF Web Service which augments the default Web Service provided in the project with a method that implements a welcome message.
    /// </summary>
    public class WelcomeService : IWelcomeService
    {
        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }

        /// <summary>
        /// Simple method that implements the interface method which returns a welcome message for the specified name and 
        /// provides the current timestamp
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public string GetWelcome(string name)
        {
            return "Welcome: " + name + ", the current timestamp is: " + DateTime.Now.ToString();
        }
    }
}
