﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week11B
{
/// <summary>
/// Basic Car Service which implements the ICarService interface.  It uses ADO.NET to pull the specified Car from the
/// Cars database.
/// </summary>
    class CarService : ICarService
    {
        /// <summary>
        /// Method that implements the interface method and uses ADO.NET to pull the specified Car from the Cars database and builds a basic description.
        /// Note the use of a SqlParameter to prevent SQL Injections.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string GetCarDescription(int id)
        {
            string description = "Not Found!";
            string connString = @"Data Source=(LocalDB)\v11.0;Initial Catalog=CSWS_S2014_Cars;Integrated Security=True";

            using (SqlConnection myConn = new SqlConnection(connString))
            {
                myConn.Open();
                SqlCommand myCmd = new SqlCommand("SELECT * FROM Cars WHERE Id = @Id", myConn);
                myCmd.Parameters.AddWithValue("@Id", id);
                SqlDataReader myRdr = myCmd.ExecuteReader();
                if (myRdr.Read())
                    description = string.Format("{0} {1} {2} {3} {4}", myRdr["Id"].ToString(), myRdr["Make"].ToString(), myRdr["Model"].ToString(), 
                                                                        myRdr["Color"].ToString(), myRdr["Year"].ToString());
            }
            return description;
        }

        /// <summary>
        /// Simple method used to demonstrate the procedure for modifying a service and using the updates.
        /// Step 1: Rebuild the service, Step 2: Update the service reference for the consuming project
        /// </summary>
        public string GetCarServiceVersion(int id)
        {
            return id.ToString();
        }
    }
}
