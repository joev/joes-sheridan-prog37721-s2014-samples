﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Week11A.Startup))]
namespace Week11A
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
