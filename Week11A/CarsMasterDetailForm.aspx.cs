﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// ASP.NET Application used to demonstrate validation of the the ASP.NET Composite controls using the Template Fields and a Master Detail
/// User Interface for car makes and details.
/// </summary>
namespace Week11A
{
    /// <summary>
    /// Class to demonstrate building a Master Detail User Interface for car makes and details.
    /// </summary>
    public partial class CarsMasterDetailForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}