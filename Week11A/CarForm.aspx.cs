﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Week11A
{
    /// <summary>
    /// Class to demonstrate adding validation to Composite Data Controls using the Template Fields and also 
    /// event handling of Data Control events.
    /// </summary>
    public partial class CarForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// DetailsView ItemInserted event handler that refreshes all of the data controls whenever the user inserts 
        /// a new entry using the DetailsView control.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DetailsView1_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
        {
            DataBind();
        }
    }
}