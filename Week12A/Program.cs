﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Console Application used to demonstrate consuming a WCF Service.  Note how the service client
/// object (carClient) is created and then used to invoke the service's methods just like any
/// other object.  The client object is the proxy to the web service and insulates us from all the 
/// "plumbing code" associated with SOAP, HTTP, WSDL, XML, etc.
/// </summary>
namespace Week12A
{
    /// <summary>
    /// Console Main Class used to demonstrate consuming a WCF Service.  Note how the service client
    /// object (carClient) is created and then used to invoke the service's methods just like any
    /// other object.  The client object is the proxy to the web service and insulates us from all the 
    /// "plumbing code" associated with SOAP, HTTP, WSDL, XML, etc.
    /// </summary>
    class Program
    {
        /// <summary>
        /// Console Main method used to demonstrate consuming a WCF Service.  It creates the service's
        /// client object and then uses it to call the two methods provided by the service which are required.
        /// It merely prints two messages to the console using the return values from the 
        /// two methods in the service.  Note that the using statement will ensure that the client object
        /// is automatically disposed of.
        /// </summary>
        static void Main(string[] args)
        {
            using (CarService.CarServiceClient carClient = new CarService.CarServiceClient())
            {
                string car = carClient.GetCarDescription(1);
                Console.WriteLine(car);
                Console.WriteLine(carClient.GetCarServiceVersion(99).ToString());
            }
        }
    }
}
