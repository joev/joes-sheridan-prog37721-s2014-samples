﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Console Application used to test and consume the Lab 2 WCF Service.  
/// </summary>
namespace Lab2C
{
    /// <summary>
    /// Console Main class used to test and consume the Lab 2 WCF Service.
    /// </summary>
    class Program
    {
        /// <summary>
        /// Main method used to test and consume the Lab 2 WCF Service.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            using (CarService.CarClient carClient = new CarService.CarClient())
            {
                // manually delete all cars from your database
                // add two new cars with a Make of your initials to the database using the service 
                CarService.Car car1 = carClient.AddCar("JV", "Model1", 2010, "Black");
                CarService.Car car2 = carClient.AddCar("JV", "Model2", 2011, "Red");

                // generate the car descriptions using the service and display them on the console
                Console.WriteLine("Added the following car: " + carClient.GetDescription(car1));
                Console.WriteLine("Added the following car: " + carClient.GetDescription(car2));

                // get all cars from the database using the service, generate the car descriptions using the service and display them on the console
                CarService.Car[] myCars = carClient.GetCars();
                Console.WriteLine("\n******* Cars in Database: ");

                // generate the car descriptions for the cars using the service and display them on the console
                foreach (CarService.Car car in myCars)
                    Console.WriteLine("Description: " + carClient.GetDescription(car));

                // update the second car in the database using the service (change the Model property)
                carClient.UpdateCar(car2.Id, "JV", "Model3", 2012, "Blue");

                // delete the first car you added in step b
                carClient.DeleteCar(car1.Id);

                // get all cars from database using the service, 
                CarService.Car[] finalCars = carClient.GetCars();

                //generate the car descriptions for the cars using the service and display them on the console
                Console.WriteLine("\n******* Final Cars in Database: ");
                foreach (CarService.Car car in finalCars)
                    Console.WriteLine("Description: " + carClient.GetDescription(car));
            }
        }
    }
}
