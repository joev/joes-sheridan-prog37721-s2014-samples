﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Week9A.Startup))]
namespace Week9A
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
