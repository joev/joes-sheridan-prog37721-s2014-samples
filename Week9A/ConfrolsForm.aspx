﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ConfrolsForm.aspx.cs" Inherits="Week9A.ConfrolsForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <table class="nav-justified">
        <tr>
            <td style="width: 127px">
                <asp:Label ID="Label1" runat="server" Text="First Name:*"></asp:Label>
            </td>
            <td style="width: 311px">
                <asp:TextBox ID="fNameTB" runat="server"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="fNameTB" ErrorMessage="First name required"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td style="width: 127px">
                <asp:Label ID="Label2" runat="server" Text="Last Name:*"></asp:Label>
            </td>
            <td style="width: 311px">
                <asp:TextBox ID="lNameTB" runat="server"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="lNameTB" ErrorMessage="Last name is required!"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td style="width: 127px">
                <asp:Label ID="Label3" runat="server" Text="Semester:*"></asp:Label>
            </td>
            <td style="width: 311px">
                <asp:DropDownList ID="semesterDDL" runat="server">
                    <asp:ListItem Value="0">Please select...</asp:ListItem>
                    <asp:ListItem>1</asp:ListItem>
                    <asp:ListItem>2</asp:ListItem>
                    <asp:ListItem>3</asp:ListItem>
                    <asp:ListItem>4</asp:ListItem>
                    <asp:ListItem>5</asp:ListItem>
                    <asp:ListItem>6</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="semesterDDL" ErrorMessage="Semester must be between 1 and 6" MaximumValue="6" MinimumValue="1" Type="Integer"></asp:RangeValidator>
            </td>
        </tr>
        <tr>
            <td style="width: 127px">
                <asp:Label ID="Label4" runat="server" Text="Birthdate:"></asp:Label>
            </td>
            <td style="width: 311px">
                <asp:Calendar ID="bDateCal" runat="server"></asp:Calendar>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 127px; height: 22px">
                <asp:Label ID="Label5" runat="server" Text="Credits:*"></asp:Label>
            </td>
            <td style="height: 22px; width: 311px">
                <asp:TextBox ID="creditsTB" runat="server" ToolTip="Enter the number of credits you currently have"></asp:TextBox>
            </td>
            <td style="height: 22px">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="creditsTB" Display="Dynamic" ErrorMessage="Credits is a required field!"></asp:RequiredFieldValidator>
                <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="creditsTB" Display="Dynamic" ErrorMessage="Credits must be between 1 and 100" MaximumValue="100" MinimumValue="1" Type="Integer"></asp:RangeValidator>
            </td>
        </tr>
        <tr>
            <td style="width: 127px; height: 22px">
                <asp:Label ID="Label6" runat="server" Text="Phone Number:*"></asp:Label>
            </td>
            <td style="height: 22px; width: 311px">
                <asp:TextBox ID="phoneTB" runat="server" ToolTip="Cell or home phone number"></asp:TextBox>
            </td>
            <td style="height: 22px">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="phoneTB" Display="Dynamic" ErrorMessage="Phone is required!"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="phoneTB" Display="Dynamic" ErrorMessage="Phone number format is invalid!" ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td style="width: 127px; height: 22px">&nbsp;</td>
            <td style="height: 22px; width: 311px">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="timeLbl" runat="server"></asp:Label>
                        <asp:Timer ID="tickTmr" runat="server" Interval="1000" OnTick="tickTmr_Tick">
                        </asp:Timer>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td style="height: 22px">&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 127px">&nbsp;</td>
            <td style="width: 311px">
                <asp:Button ID="submitBtn" runat="server" Text="Submit" />
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 127px">&nbsp;</td>
            <td style="width: 311px">
                <asp:Label ID="statusLbl" runat="server"></asp:Label>
            </td>
            <td>&nbsp;</td>
        </tr>
    </table>
</asp:Content>
