﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// ASP.NET Application used to demonstrate many of the ASP.NET Standard Controls (TextBox, Label, DropDownList, Button, Calendar, etc.); 
/// Validation Controls (RequiredFieldValidator, RangeValidator, etc.) and AJAX Controls (Timer, UpdatePanel).  
/// It also demonstrates how to align controls using tables and how to assist the user with ToolTips.  Finally, it shows how to detect 
/// whether or not a Page is loaded due to a Postback and how we might handle the Page Load event differently depending on whether or not a Postback
/// has occurred.
/// </summary>
namespace Week9A
{
    /// <summary>
    /// ASP.NET page used to demonstrate many of the ASP.NET Standard Controls (TextBox, Label, DropDownList, Button, Calendar, etc.); 
    /// Validation Controls (RequiredFieldValidator, RangeValidator, etc.) and AJAX Controls (Timer, UpdatePanel).  
    /// It also demonstrates how to align controls using tables and how to assist the user with ToolTips.  Finally, it shows how to detect 
    /// whether or not a Page is loaded due to a Postback and how we might handle the Page Load event differently depending on whether or not a Postback
    /// has occurred.
    /// </summary>
    public partial class ConfrolsForm : System.Web.UI.Page
    {
        /// <summary>
        /// Page load handler which detects whether the page was loaded due to a Postback and displays
        /// corresponding messages.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
                statusLbl.Text = "Page loaded due to postback!";
            else
                statusLbl.Text = "Fresh load.  Page load NOT due to postback!";
        }

        /// <summary>
        /// AJAX Timer handler which updates the Time Of Day label on the page every second with the current time.  Note that this update
        /// does NOT reload the entire page as a standard Postback would but rather completes using a Partial Page refresh which is much faster!
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tickTmr_Tick(object sender, EventArgs e)
        {
            timeLbl.Text = DateTime.Now.ToString();
        }
    }
}