﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CompositeControlsForm.aspx.cs" Inherits="Week10B.CompositeControlsForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <table class="nav-justified">
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="DetailsView:"></asp:Label>
            </td>
            <td>
                <asp:DetailsView ID="DetailsView1" runat="server" Height="50px" Width="125px">
                </asp:DetailsView>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label2" runat="server" Text="FormView:"></asp:Label>
            </td>
            <td>
                <asp:FormView ID="FormView1" runat="server">
                </asp:FormView>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label3" runat="server" Text="GridView:"></asp:Label>
            </td>
            <td>
                <asp:GridView ID="GridView1" runat="server">
                </asp:GridView>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label4" runat="server" Text="ListView:"></asp:Label>
            </td>
            <td>
                <asp:ListView ID="ListView1" runat="server">
                </asp:ListView>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label5" runat="server" Text="Repeater:"></asp:Label>
            </td>
            <td>
                <asp:Repeater ID="Repeater1" runat="server">
                </asp:Repeater>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label6" runat="server" Text="DataList:"></asp:Label>
            </td>
            <td>
                <asp:DataList ID="DataList1" runat="server">
                </asp:DataList>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <asp:Button ID="submitBtn" runat="server" OnClick="submitBtn_Click" Text="Submit" />
            </td>
            <td>&nbsp;</td>
        </tr>
    </table>
</asp:Content>
