﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// ASP.NET Application used to demonstrate data binding to the ASP.NET WebList controls (BulletedList, CheckBoxList, DropDownList, ListBox
/// and RadioButtonList) and the ASP.NET Composite controls using the Declarative Approach (Drag and Drop).  This demo uses all of the composite controls for 
/// single records (DetailsView and FormView) as well as all of the composite controls for multiple records (GridView, ListView, DataList 
/// and Repeater).  This application demonstrates what each of the controls look like, how we can style them using the SmartTasks for each
/// control in the Designer, how we can add Validators to them and how we can bind them to a SQL database using a non-visual SqlDataSource control.
/// It also demonstrates how to add an application setting to the web.config file and how to access this setting at runtime.
/// </summary>
namespace Week10B
{
    /// <summary>
    /// Class used to demonstrate all of the composite controls for single records (DetailsView and FormView) as well as all of the composite 
    /// controls for multiple records (GridView, ListView, DataList and Repeater).  Note that the Declarative Approach (Drag and Drop from Server Explorer) 
    /// is used here.
    public partial class CompositeDeclarativeForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}