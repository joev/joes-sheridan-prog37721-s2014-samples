﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Week10B.Startup))]
namespace Week10B
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
