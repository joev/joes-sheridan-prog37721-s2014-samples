﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// ASP.NET Application used to demonstrate data binding to the ASP.NET WebList controls (BulletedList, CheckBoxList, DropDownList, ListBox
/// and RadioButtonList) and the ASP.NET Composite controls using the Declarative Approach (Drag and Drop) as well as the Programmatic Approach 
/// (C# ADO.NET in code behind file).  This demo uses all of the composite controls for 
/// single records (DetailsView and FormView) as well as all of the composite controls for multiple records (GridView, ListView, DataList 
/// and Repeater).  This application demonstrates what each of the controls look like, how we can style them using the SmartTasks for each
/// control in the Designer, how we can add Validators to them and how we can bind them to a SQL database using a non-visual SqlDataSource control.
/// It also demonstrates how to add an application setting to the web.config file and how to access this setting at runtime.
/// </summary>
namespace Week10B
{
    /// <summary>
    /// Class to demonstrate data binding to the ASP.NET WebList controls (BulletedList, CheckBoxList, DropDownList, ListBox
    /// and RadioButtonList) using the Programmatic Approach (C# ADO.NET in code behind file).  
    /// </summary>
    public partial class WebListForm : System.Web.UI.Page
    {
        /// <summary>
        /// Sets up the data bindings for the WebList controls on fresh page loads (NOT on postbacks)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                SetupDataBindings();
        }

        /// <summary>
        /// Binds an array of cars to the various WebList controls using the Programmatic Approach (C# ADO.NET in code behind file)
        /// </summary>
        private void SetupDataBindings()
        {
            Car[] myCars = { new Car {Id = 1, Make = "Honda", Model = "Accord", Color = "Black", Year = 2010},
                             new Car {Id = 2, Make = "Honda", Model = "Civic", Color = "Red", Year = 2011},
                             new Car {Id = 3, Make = "Cadillac", Model = "Escalade", Color = "White", Year = 2012}};

            BulletedList1.DataSource = myCars;
            BulletedList1.DataTextField = "Description";
            BulletedList1.DataValueField = "Id";

            CheckBoxList1.DataSource = myCars;
            CheckBoxList1.DataTextField = "Description";
            CheckBoxList1.DataValueField = "Id";

            DropDownList1.DataSource = myCars;
            DropDownList1.DataTextField = "Description";
            DropDownList1.DataValueField = "Id";

            ListBox1.DataSource = myCars;
            ListBox1.DataTextField = "Description";
            ListBox1.DataValueField = "Id";

            RadioButtonList1.DataSource = myCars;
            RadioButtonList1.DataTextField = "Description";
            RadioButtonList1.DataValueField = "Id";

            DataBind();
        }
        protected void submitBtn_Click(object sender, EventArgs e)
        {

        }
    }

    /// <summary>
    /// Simple Car class used to demonstrate data binding
    /// </summary>
    public class Car
    {
        public int Id { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Color { get; set; }
        public int Year { get; set; }
        public string Description { get { return Make + " " + Model + " " + Year; } }
    }
}