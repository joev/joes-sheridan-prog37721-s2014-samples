﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="WebListForm.aspx.cs" Inherits="Week10B.WebListForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <table class="nav-justified">
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="BulletedList:"></asp:Label>
            </td>
            <td>
                <asp:BulletedList ID="BulletedList1" runat="server">
                </asp:BulletedList>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label2" runat="server" Text="CheckBoxList:"></asp:Label>
            </td>
            <td>
                <asp:CheckBoxList ID="CheckBoxList1" runat="server">
                </asp:CheckBoxList>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label3" runat="server" Text="DropDownList:"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True">
                </asp:DropDownList>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="height: 22px">
                <asp:Label ID="Label4" runat="server" Text="ListBox:"></asp:Label>
            </td>
            <td style="height: 22px">
                <asp:ListBox ID="ListBox1" runat="server"></asp:ListBox>
            </td>
            <td style="height: 22px"></td>
        </tr>
        <tr>
            <td style="height: 24px">
                <asp:Label ID="Label5" runat="server" Text="RadioButtonList:"></asp:Label>
            </td>
            <td style="height: 24px">
                <asp:RadioButtonList ID="RadioButtonList1" runat="server">
                </asp:RadioButtonList>
            </td>
            <td style="height: 24px"></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <asp:Button ID="submitBtn" runat="server" OnClick="submitBtn_Click" Text="Submit" />
            </td>
            <td>&nbsp;</td>
        </tr>
    </table>
</asp:Content>
