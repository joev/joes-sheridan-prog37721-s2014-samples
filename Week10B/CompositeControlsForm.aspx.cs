﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Week10B
{
    /// <summary>
    /// Class to demonstrate Programmatic Data Binding to the Composite Data Controls.  Notice that the Programmatic
    /// approach utilizes standard ADO.NET code to do the queries and to set up the data bindings.
    /// </summary>
    public partial class CompositeControlsForm : System.Web.UI.Page
    {
        /// <summary>
        /// Method to set up the data bindings for the Composite Data Controls on fresh page loads (no PostBacks).  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                SetupDataBindings();
        }

        /// <summary>
        /// Method to query the database and set up the data bindings for the Composite Data Controls.  Notice that standard ADO.NET code is used
        /// here.  Notice the explicit call to the DataBind method which is NOT required in Windows Forms.
        /// </summary>
        private void SetupDataBindings()
        {
            string connString = @"Data Source=(LocalDB)\v11.0;Initial Catalog=CSWS_S2014_Cars;Integrated Security=True";
            DataSet ds = new DataSet();

            using (SqlConnection conn = new SqlConnection(connString))
            {
                SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM Cars", conn);
                da.Fill(ds, "Cars");
            }

            GridView1.DataSource = ds;
            DetailsView1.DataSource = ds;
            //ListView1.DataSource = ds;
            FormView1.DataSource = ds;
            Repeater1.DataSource = ds;
            DataList1.DataSource = ds;

            DataBind();
        }

        protected void submitBtn_Click(object sender, EventArgs e)
        {

        }
    }
}