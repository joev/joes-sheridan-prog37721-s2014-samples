﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Week9B
{
    /// <summary>
    /// Web form used to define a simple student interface.  We styled the label, textbox and validation control setting the CssClass property of these controls to
    /// the custom CSS class names that we added to the Site.css file
    /// </summary>
    public partial class StudentForm : System.Web.UI.Page
    {
        /// <summary>
        /// Created just to demonstrate the page loading sequence.  No logic added.
        /// </summary>
        public StudentForm() : base() { }

        /// <summary>
        /// Created just to demonstrate the page loading sequence.  No logic added.
        /// </summary>
        public override void Validate()
        {
            base.Validate();
        }

        /// <summary>
        /// Created just to demonstrate the page loading sequence.  No logic added.
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Intentionally causes an unhandled exception to demonstrate error handling using the Application_Error method in the Global.asax file.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void submitBtn_Click(object sender, EventArgs e)
        {
            int divisor = 0;
            int result = 100 / divisor;
        }

        /// <summary>
        /// Created just to demonstrate the page loading sequence.  No logic added.
        /// </summary>
        protected void fNameTB_TextChanged(object sender, EventArgs e)
        {

        }
    }
}