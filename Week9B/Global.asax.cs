﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;

/// <summary>
/// ASP.NET Application used to demonstrate styling ASP.NET Controls using CSS and how to change the navigation bar, site title and logo of the
/// entire site using the Site.Master file.  Finally we saw how to handle unhandled errors using the Application_Error method of the 
/// Global.asax file
/// </summary>
namespace Week9B
{
    /// <summary>
    /// Class used to define Application Start, Application End, Application Error, Session Start and Session End handlers.  We customized the Application 
    /// Error handler to redirect all errors to the About page temporarily. In the next class we will redirect to a customized error page with the 
    /// details of the error which is preferable to the user seeing an unbranded and confusing 404 or similar Browser error.
    /// </summary>
    public class Global : HttpApplication
    {

        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        void Application_End(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Used to redirect to our About page.  In the next class we will redirect to a customized error page with the 
        /// details of the error which is preferable to the user seeing an unbranded and confusing 404 or similar Browser error.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Application_Error(object sender, EventArgs e)
        {
            string error = Server.GetLastError().ToString();
            Response.Redirect("About.aspx");
        }

        void Session_Start(object sender, EventArgs e)
        {

        }
        void Session_End(object sender, EventArgs e)
        {

        }
    }
}