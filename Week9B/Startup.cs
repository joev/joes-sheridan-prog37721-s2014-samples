﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Week9B.Startup))]
namespace Week9B
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
