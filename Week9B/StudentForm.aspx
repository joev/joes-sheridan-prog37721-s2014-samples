﻿<%@ Page Title="" Language="C#" Trace="true" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="StudentForm.aspx.cs" Inherits="Week9B.StudentForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <table class="nav-justified">
        <tr>
            <td style="width: 156px">
                <asp:Label ID="Label1" runat="server" CssClass="myLabel" Text="First Name:*"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="fNameTB" runat="server" CssClass="myTextbox" OnTextChanged="fNameTB_TextChanged"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="fNameTB" CssClass="myValidationMsg" ErrorMessage="First name is required!"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td style="width: 156px">&nbsp;</td>
            <td>
                <asp:Button ID="submitBtn" runat="server" CssClass="myLabel" Text="Submit" OnClick="submitBtn_Click" />
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 156px">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 156px">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 156px">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
</asp:Content>
