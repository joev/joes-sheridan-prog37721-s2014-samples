﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Lab2
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    /// <summary>
    /// Defines the interface for the WCF Car Service
    /// </summary>
    [ServiceContract]
    public interface ICar
    {
        // Add the specified car to the DB and return the integer ID of the newly added Car or zero on failure
        [OperationContract]
        Car AddCar(string make, string model, int year, string color);

        // Fetch a list of all Cars in the database and return the list of Cars or null on failure
        [OperationContract]
        List<Car> GetCars();

        // Fetch the specified Car from the DB and return a Car object based on it or null on failure
        [OperationContract]
        Car GetCar(int id);

        // Update the specified Car in the DB return the integer ID of the updated Car in the database or zero on failure
        [OperationContract]
        int UpdateCar(int id, string make, string model, int year, string color);

        // Delete the specified Car in the DB and return the integer ID of the deleted Car or zero on failure.
        [OperationContract]
        int DeleteCar(int id);

        // Builds a string description which lists all properties of the supplied Car 
        [OperationContract]
        string GetDescription(Car car);
    }

    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    // You can add XSD files into the project. After building the project, you can directly use the data types defined there, with the namespace "Week11B.ContractType".
    /// <summary>
    /// Defines a composite data type for Car objects that is used by the WCF service.
    /// </summary>
    [DataContract]
    public class Car
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Make { get; set; }

        [DataMember]
        public string Model { get; set; }

        [DataMember]
        public int Year { get; set; }

        [DataMember]
        public string Color { get; set; }
    }
}
