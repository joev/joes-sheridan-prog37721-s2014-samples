﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// WCF Service Library used to implement the WCF Service for Lab 2.  It demonstrates providing full CRUD 
/// support for a Car object.
/// </summary>
namespace Lab2
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    /// <summary>
    /// Car Service class which implements the ICar Service interface
    /// </summary>
    public class CarService : ICar
    {
        private string connString = @"Data Source=(LocalDB)\v11.0;Initial Catalog=CSWS_S2014_Cars;Integrated Security=True";

        /// <summary>
        /// Adds a database record with the supplied car fields
        /// </summary>
        /// <param name="make"></param>
        /// <param name="model"></param>
        /// <param name="year"></param>
        /// <param name="color"></param>
        /// <returns></returns>
        public Car AddCar(string make, string model, int year, string color)
        {
            Car newCar = null;
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                string query = "INSERT INTO Cars (Make, Model, Year, Color) VALUES (@Make, @Model, @Year, @Color); "
                                + "SELECT CAST(scope_identity() AS int)";
                SqlCommand myCmd = new SqlCommand(query, conn);
                myCmd.Parameters.AddWithValue("@Make", make);
                myCmd.Parameters.AddWithValue("@Model", model);
                myCmd.Parameters.AddWithValue("@Year", year);
                myCmd.Parameters.AddWithValue("@Color", color);

                int id = (int)myCmd.ExecuteScalar();
                newCar = GetCar(id);
            }
            return newCar;
        }

        /// <summary>
        /// Retrieves the specified Car record from the database based on the ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Car GetCar(int id)
        {
            Car theCar = null;
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                SqlCommand myCmd = new SqlCommand("SELECT * from Cars WHERE Id = @Id", conn);
                myCmd.Parameters.AddWithValue("@Id", id);
                SqlDataReader myRdr = myCmd.ExecuteReader();
                if (myRdr.Read())
                {
                    theCar = new Car
                    {
                        Id = (int)myRdr["Id"],
                        Make = myRdr["Make"].ToString(),
                        Model = myRdr["Model"].ToString(),
                        Year = (int)myRdr["Year"],
                        Color = myRdr["Color"].ToString()
                    };
                }
            }
            return theCar;
        }

        /// <summary>
        /// Returns a list of Cars based on the Car table in the database
        /// </summary>
        /// <returns></returns>
        public List<Car> GetCars()
        {
            List<Car> cars = new List<Car>();

            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                SqlCommand myCmd = new SqlCommand("SELECT * from Cars", conn);
                SqlDataReader myRdr = myCmd.ExecuteReader();
                while (myRdr.Read())
                {
                    Car theCar = new Car
                    {
                        Id = (int)myRdr["Id"],
                        Make = myRdr["Make"].ToString(),
                        Model = myRdr["Model"].ToString(),
                        Year = (int)myRdr["Year"],
                        Color = myRdr["Color"].ToString()
                    };
                    cars.Add(theCar);
                }
            }

            return cars;
        }

        /// <summary>
        /// Updates the specified Car in the database with the supplied fields
        /// </summary>
        /// <param name="id"></param>
        /// <param name="make"></param>
        /// <param name="model"></param>
        /// <param name="year"></param>
        /// <param name="color"></param>
        /// <returns></returns>
        public int UpdateCar(int id, string make, string model, int year, string color)
        {
            int result = 0;
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                string query = "UPDATE Cars SET Make=@Make, Model=@Model, Year=@Year, Color=@Color "
                                + "WHERE Id=@id";
                SqlCommand myCmd = new SqlCommand(query, conn);
                myCmd.Parameters.AddWithValue("@Make", make);
                myCmd.Parameters.AddWithValue("@Model", model);
                myCmd.Parameters.AddWithValue("@Year", year);
                myCmd.Parameters.AddWithValue("@Color", color);
                myCmd.Parameters.AddWithValue("@Id", id);

                result = myCmd.ExecuteNonQuery();
            }
            return result;
        }

        /// <summary>
        /// Deletes the specified Car from the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public int DeleteCar(int id)
        {
            int result = 0;
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                SqlCommand myCmd = new SqlCommand("DELETE from Cars WHERE Id = @Id", conn);
                myCmd.Parameters.AddWithValue("@Id", id);
                result = myCmd.ExecuteNonQuery();
            }
            return result;
        }

        /// <summary>
        /// Builds a Car description string based on the supplied car object
        /// </summary>
        /// <param name="car"></param>
        /// <returns></returns>
        public string GetDescription(Car car) {
            return string.Format("{0} {1} {2} {3}", car.Id, car.Make, car.Model, car.Year, car.Color);
        }


        /// <summary>
        /// Returns a List of Cars from the database which are filtered by the supplied make
        /// </summary>
        /// <param name="make"></param>
        /// <returns></returns>
        public List<Car> GetCarsByMake(string make)
        {
            List<Car> cars = new List<Car>();

            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                SqlCommand myCmd = new SqlCommand("SELECT * from Cars WHERE Make=@Make", conn);
                myCmd.Parameters.AddWithValue("@Make", make);
                SqlDataReader myRdr = myCmd.ExecuteReader();
                while (myRdr.Read())
                {
                    Car theCar = new Car
                    {
                        Id = (int)myRdr["Id"],
                        Make = myRdr["Make"].ToString(),
                        Model = myRdr["Model"].ToString(),
                        Year = (int)myRdr["Year"],
                        Color = myRdr["Color"].ToString()
                    };
                    cars.Add(theCar);
                }
            }
            return cars;
        }

        /// <summary>
        /// Returns a list of car makes based on the cars in the database.
        /// </summary>
        /// <returns></returns>
        public List<string> GetCarMakes()
        {
            List<string> makes = new List<string>();

            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                SqlCommand myCmd = new SqlCommand("SELECT Distinct Make from Cars", conn);
                SqlDataReader myRdr = myCmd.ExecuteReader();
                while (myRdr.Read())
                    makes.Add(myRdr["Make"].ToString());
            }
            return makes;
        }
    }
}
