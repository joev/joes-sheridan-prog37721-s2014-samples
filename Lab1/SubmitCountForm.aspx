﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SubmitCountForm.aspx.cs" Inherits="Lab1.SubmitCountForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <table class="nav-justified">
    <tr>
        <td style="width: 158px">&nbsp;</td>
        <td>
            &nbsp;</td>
    </tr>
    <tr>
        <td style="width: 158px">
            <asp:Label ID="Label1" runat="server" Text="ViewState Count:"></asp:Label>
        </td>
        <td>
            <asp:Label ID="viewStateLbl" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td style="width: 158px">
            <asp:Label ID="Label2" runat="server" Text="Session Count:"></asp:Label>
        </td>
        <td>
            <asp:Label ID="sessionLbl" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td style="width: 158px">
            <asp:Label ID="Label3" runat="server" Text="Query String Count:"></asp:Label>
        </td>
        <td>
            <asp:Label ID="qsLbl" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td style="width: 158px">
            &nbsp;</td>
        <td>
            <asp:Button ID="submitBtn" runat="server" OnClick="submitBtn_Click" Text="Submit" />
            <asp:Button ID="redirectBtn" runat="server" OnClick="redirectBtn_Click" Text="Redirect" />
        </td>
    </tr>
</table>
<br />
<br />
<br />
<br />
<br />
</asp:Content>
