﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// ASP.NET Application for Lab 1.  It demonstrates how to use ASP.NET State Management techniques (ViewState, Session, Query Strings)
/// to maintain state information.
/// </summary>
namespace Lab1
{
    /// <summary>
    /// ASP.NET Web Form for Lab 1 that demonstrates how to use ASP.NET State Management techniques (ViewState, Session, Query Strings)
    /// to maintain state information.
    /// </summary>
    public partial class SubmitCountForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (ViewState["submits"] == null)
              ViewState["submits"] = 0;
            viewStateLbl.Text = ViewState["submits"].ToString();

            if (Session["submits"] == null)
              Session["submits"] = 0;
            sessionLbl.Text = Session["submits"].ToString();

            if (Request.QueryString["redirects"] == null)
              qsLbl.Text = "0";
            else
              qsLbl.Text = Request.QueryString["redirects"].ToString();
        }

        /// <summary>
        /// Method used to maintain the count of submit clicks using both the ViewState and Session collections
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void submitBtn_Click(object sender, EventArgs e)
        {
            ViewState["submits"] = (int)ViewState["submits"] + 1;
            Session["submits"] = (int)Session["submits"] + 1;

            viewStateLbl.Text = ViewState["submits"].ToString();
            sessionLbl.Text = Session["submits"].ToString();
        }

        /// <summary>
        /// Method used to maintain a count of the number of Redirect button clicks using a Query String.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void redirectBtn_Click(object sender, EventArgs e)
        {
            int count = 0;
            if (Request.QueryString["redirects"] != null)
            {
                if (Int32.TryParse(Request.QueryString["redirects"], out count) == false)
                    return;
            }
            Response.Redirect("SubmitCountForm.aspx?redirects=" + ++count, false);
        }
    }
}